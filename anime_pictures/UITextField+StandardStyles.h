//
//  UITextField+StandardStyles.h
//  anime_pictures
//
//  Created by iButs on 28.09.14.
//  Copyright (c) 2014 anime_pictures_net. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (StandardStyles)
-(void)setStandardStyles;
@end

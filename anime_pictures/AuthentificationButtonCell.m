//
//  AuthentificationButtonCell.m
//  anime_pictures
//
//  Created by iButs on 30.09.14.
//  Copyright (c) 2014 anime_pictures_net. All rights reserved.
//

#import "AuthentificationButtonCell.h"
#import "UIButton+StandardStyles.h"
#import "UserInterfaceConstants.h"

@implementation AuthentificationButtonCell

@synthesize authentificationButton = _authentificationButton;

-(instancetype)init{
    self = [super init];
    [self addSubview:self.authentificationButton];
    [self addConstraintsForAuthentificationButton];
    return self;
}

-(UIButton *)authentificationButton{
    if (!_authentificationButton){
        _authentificationButton = [[UIButton alloc]initWithFrame:CGRectNull];
        [_authentificationButton setStandardStylesForButton];
        _authentificationButton.translatesAutoresizingMaskIntoConstraints = NO;
        [_authentificationButton setTitle:@"Authentification" forState:UIControlStateNormal];
    }
    return _authentificationButton;
}

-(void)addConstraintsForAuthentificationButton{
    NSLayoutConstraint *width = [NSLayoutConstraint constraintWithItem:self.authentificationButton attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeWidth multiplier:1.0 constant:-HORIZONTAL_PADDING];
    NSLayoutConstraint *height = [NSLayoutConstraint constraintWithItem:self.authentificationButton attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:ELEMENTS_HEIGHT];
    NSLayoutConstraint *centerX = [NSLayoutConstraint constraintWithItem:self.authentificationButton attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0];
    NSLayoutConstraint *bottom = [NSLayoutConstraint constraintWithItem:self.authentificationButton attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-5];
    [self addConstraints:@[width, height, centerX, bottom]];
}

@end

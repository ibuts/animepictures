//
//  SearchViewController.m
//  anime_pictures
//  Created by iButs on 24.09.14.
//  Copyright (c) 2014 anime_pictures_net. All rights reserved.
//

#import "SearchViewController.h"
#import "UIButton+StandardStyles.h"
#import "SearchFiltersCell.h"
#import "SearchBarCell.h"
#import "SearchButtonCell.h"
#import "LoginButtonCell.h"
#import "AuthentificationButtonCell.h"

@interface SearchViewController (){
    BOOL favoritesIsOpen;
    BOOL searchIsOpen;
    BOOL settingsIsOpen;
    BOOL userIsLogin;
}

@property (strong, nonatomic) SearchFiltersCell *searchFiltersCell;
@property (strong, nonatomic) SearchButtonCell *searchButtonCell;
@property (strong, nonatomic) SearchBarCell *searchBarCell;
@property (strong, nonatomic) LoginButtonCell *loginButtonCell;
@property (strong, nonatomic) AuthentificationButtonCell *authentificationButtonCell;
@property (strong, nonatomic) UIButton *searchButton;
@property (strong, nonatomic) UISearchBar *searchBar;
@property (strong, nonatomic) NSArray *userFoldersArray;
@property (strong, nonatomic) NSString *selectedColor;

@end


@implementation SearchViewController

@synthesize tableView = _tableView;
@synthesize searchView = _searchView;
@synthesize searchFiltersCell = _searchFiltersCell;
@synthesize colorController = _colorController;
@synthesize searchButton = _searchButton;
@synthesize searchBar = _searchBar;
@synthesize searchBarCell = _searchBarCell;
@synthesize searchButtonCell = _searchButtonCell;
@synthesize loginButtonCell = _loginButtonCell;
@synthesize authentificationButtonCell = _authentificationButtonCell;
@synthesize connectionManager;


- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview: self.tableView];
    [self addConstraintsForTableView];
    self.connectionManager.loginDelegate = self;
    favoritesIsOpen = 0;
    searchIsOpen = 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(FCColorPickerViewController *)colorController{
    if (!_colorController){
        _colorController = [FCColorPickerViewController colorPickerWithColor:[UIColor whiteColor] delegate:self];
        _colorController.view.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _colorController;
}

-(ConnectionManager *)connectionManager{
    return [ConnectionManager sharedManager];
}

-(SearchFiltersCell *)searchFiltersCell{
    if (!_searchFiltersCell){
        _searchFiltersCell = [[SearchFiltersCell alloc]initWithColorView:self.colorController.view];
        _searchFiltersCell.searchView.searchBar.delegate = self;
        _searchFiltersCell.searchView.widthField.delegate = self;
        _searchFiltersCell.searchView.heightField.delegate = self;
        [_searchFiltersCell.searchView.autosizeButton addTarget:self action:@selector(autosizeButtonDidPressed:) forControlEvents:UIControlEventTouchDown];
    }
    return _searchFiltersCell;
}


-(SearchBarCell *)searchBarCell{
    if (!_searchBarCell){
        _searchBarCell = [[SearchBarCell alloc]init];
        _searchBarCell.searchBar.delegate = self;
    }
    return _searchBarCell;
}

-(SearchButtonCell *)searchButtonCell{
    if (!_searchButtonCell)
        _searchButtonCell = [[SearchButtonCell alloc]init];
    [_searchButtonCell.searchButton addTarget:self action:@selector(searchButtonDidPressed:) forControlEvents:UIControlEventTouchDown];
    return _searchButtonCell;
}

-(LoginButtonCell *)loginButtonCell{
    if (!_loginButtonCell)
        _loginButtonCell = [[LoginButtonCell alloc]init];
    [_loginButtonCell.loginButton addTarget:self action:@selector(loginButtonDidPressed:) forControlEvents:UIControlEventTouchDown];
    return _loginButtonCell;
}

-(AuthentificationButtonCell *)authentificationButtonCell{
    if (!_authentificationButtonCell)
        _authentificationButtonCell = [[AuthentificationButtonCell alloc]init];
    return _authentificationButtonCell;
}

-(UITableView *)tableView{
    if (!_tableView){
        _tableView = [[UITableView alloc]initWithFrame:CGRectNull];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _tableView;
}

-(void)addConstraintsForTableView{
    NSLayoutConstraint *width = [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0.0];
    NSLayoutConstraint *height = [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeHeight multiplier:1.0 constant:0.0];
    NSLayoutConstraint *centerX = [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0];
    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0];
    [self.view addConstraints:@[width, height, centerX, top]];
}

#pragma mark - TableViewDataSourceeMethods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 5;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 1){
        if (searchIsOpen)
            return 2;
        return 1;
    }
    if ((section == 3) && (favoritesIsOpen))
        return userIsLogin ? self.userFoldersArray.count + 2 : 3;
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [[UITableViewCell alloc]init];
    if ((indexPath.section == 0)&&(indexPath.row == 0))
        return self.searchBarCell;
    if (indexPath.section == 1){
        if (indexPath.row == 0)
            cell.textLabel.text = !searchIsOpen ? @"Advanced search" : @"Standard search";
        if (indexPath.row == 1)
            return self.searchFiltersCell;
    }
    if (indexPath.section == 2)
        return self.searchButtonCell;
    if (indexPath.section == 3){
        if (indexPath.row!=0){
            if (!userIsLogin){
                if (indexPath.row == 1)
                    return self.loginButtonCell;
                if (indexPath.row == 2)
                    return self.authentificationButtonCell;
            }else{
                if (indexPath.row == 1)
                    cell.textLabel.text = @"Show all";
                else
                cell.textLabel.text = [self.userFoldersArray objectAtIndex:indexPath.row-2];
            }
        }
        if (indexPath.row == 0)
            cell.textLabel.text = !favoritesIsOpen ? @"Show my favorites" : @"Hide my favorites";
    }
    if (indexPath.section == 4)
        cell.textLabel.text = @"Settings";
    return cell;
}

#pragma mark - tableViewDelegateMethods 

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ((indexPath.section == 1)&&(indexPath.row == 0)){
        searchIsOpen =! searchIsOpen;
        [_tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    if (indexPath.section == 3){
        if (indexPath.row == 0){
            favoritesIsOpen =! favoritesIsOpen;
            [_tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationAutomatic];
        }else{
            
            [self.delegate didSelectedFolderWithName:indexPath.row == 1? @"showAll" : [self.userFoldersArray objectAtIndex:indexPath.row-2]];
//            [self.connectionManager downloadingPathesForFavoritesWithPageNumber:0 andFavoriteFolderName: indexPath.row == 1? @"" : [self.userFoldersArray objectAtIndex:indexPath.row-2]];
        }
    }
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0)
        return ELEMENTS_HEIGHT+25;
    if (indexPath.section == 1){
        if (indexPath.row == 1)
            return  520;
    }
    if ((indexPath.section == 2) || ((indexPath.section == 3)&&(indexPath.row!=0)))
        return ELEMENTS_HEIGHT+10;
    return ELEMENTS_HEIGHT;
}

-(void)colorPickerViewController:(FCColorPickerViewController *)colorPicker didSelectColor:(UIColor *)color{
    CGColorRef colorRef = color.CGColor;
    self.selectedColor = [CIColor colorWithCGColor:colorRef].stringRepresentation;
}

-(void)colorPickerViewControllerDidCancel:(FCColorPickerViewController *)colorPicker{
    
}

-(void)searchButtonDidPressed:(UIButton*)button{
    if (self.searchBarCell.searchBar.text.length > 0)
        [self toSearch];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    if (searchBar.text.length > 0)
        [self toSearch];
}

-(void)toSearch{
    NSString *searchTagString = [self.searchBarCell.searchBar.text stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    NSString *howRecent = [NSString stringWithFormat:@"%ld",(long)self.searchFiltersCell.searchView.howRecentControl.selectedSegmentIndex];
    NSString *howSorting = [self.searchFiltersCell.searchView.howSortingControl titleForSegmentAtIndex:self.searchView.howSortingControl.selectedSegmentIndex].lowercaseString;

    
    NSString *width = self.searchFiltersCell.searchView.widthField.text ? self.searchFiltersCell.searchView.widthField.text : @"";
    NSString *height = self.searchFiltersCell.searchView.heightField.text ? self.searchFiltersCell.searchView.heightField.text : @"";
    NSString *color = self.selectedColor ? self.selectedColor : @"";
    [self.delegate searchButtonDidPressedForSearchWithDictionary:@{
                                                                   @"page_number" : @"0",
                                                                   @"search_tag" : searchTagString,
                                                                   @"how_recent" : howRecent ? howRecent : @"",
                                                                   @"how_sorting" : howSorting ? howSorting : @"",
                                                                   @"width" : width,
                                                                   @"height" : height,
                                                                   @"color" : color
                                                                   }];
    [self.searchBarCell.searchBar resignFirstResponder];
}

-(void)loginButtonDidPressed:(UIButton*)button{
    if (!self.connectionManager.userID)
        [self.connectionManager loginUserWithName:@"ibuts" andPassword:@"9379992buts"];
    else
        [self.delegate didSelectedFolderWithName:@"showAll"];
//        [self.connectionManager downloadingPathesForFavoritesWithPageNumber:0 andFavoriteFolderName:nil];
}

-(void)userDidLogin:(BOOL)idIsExist{
    userIsLogin = idIsExist;
}

-(void)userFoldersDidParsing:(NSArray *)foldersArray{
    self.userFoldersArray = foldersArray;
    favoritesIsOpen = 0;
    dispatch_async(dispatch_get_main_queue(), ^
    {
        [_tableView reloadSections:[NSIndexSet indexSetWithIndex:3] withRowAnimation:UITableViewRowAnimationFade];
    });
}

-(void)autosizeButtonDidPressed:(UIButton*)button{
    CGRect screenSize = [[UIScreen mainScreen] bounds];
    self.searchFiltersCell.searchView.widthField.text = [NSString stringWithFormat:@"%.0f", screenSize.size.width];
    self.searchFiltersCell.searchView.heightField.text = [NSString stringWithFormat:@"%.0f", screenSize.size.height];
}

@end

//
//  BTSSearchView.h
//  anime_pictures
//
//  Created by iButs on 25.09.14.
//  Copyright (c) 2014 anime_pictures_net. All rights reserved.
//
#define HORIZONTAL_PADDING 20.0f
#define VERTICAL_PADDING 10.0f
#define ELEMENTS_HEIGHT 40.0f

#import <UIKit/UIKit.h>

@interface SearchView : UIView
@property (strong, nonatomic) UISearchBar *searchBar;
@property (strong, nonatomic) UISegmentedControl *howRecentControl;
@property (strong, nonatomic) UISegmentedControl *howSortingControl;
@property (strong, nonatomic) UITextField *widthField;
@property (strong, nonatomic) UITextField *heightField;
@property (strong, nonatomic) UIButton *autosizeButton;
@property (strong, nonatomic) UIButton *searchButton;
@property (strong, nonatomic) UIView *colorView;

@end


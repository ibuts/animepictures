//
//  ConnectionManager.m
//  Anime-pictures
//
//  Created by iButs on 10.04.14.
//  Copyright (c) 2014 iButs. All rights reserved.
//

#import "ConnectionManager.h"
#import "PostObject.h"

#define URL_ROOT_PART                           @"http://anime-pictures.net/"
#define URL_POST_LOGINIG                        @"login/submit"
#define URL_FAVORITE_BY_PART                    @"&favorite_by="
#define URL_FAVORITES_IMAGES_FROM_FOLDER_PART   @"&favorite_folder="
#define URL_POSTS_PER_PAGE_PART                 @"&posts_per_page="
#define URL_SEARCH_TAG_PART                     @"&search_tag="
#define URL_TAG_PATHES_P_2                      @"?lang=en&posts_per_page=200&type=json&search_tag="
#define URD_DENIED_TAGS_PART                    @"&denied_tags=erotic"
#define URL_RES_X_PART                          @"&res_x="
#define URL_RES_Y_PART                          @"&res_y="
#define URL_ORDER_BY_PART                       @"&order_by="
#define URL_DATE_PART                           @"&ldate="
#define URL_PICTURES_VIEW_POSTS_PART            @"pictures/view_posts/"
#define URL_PICTURES_VIEW_POST_PART             @"pictures/view_post/"
#define URL_LANG_PART                           @"?lang=en&type=json"

#define URL_POST_SET_RAITING                    @"pictures/vote"
#define URL_POST_FAVORITE_FOLDER_P_1            @"pictures/set_favorites/"
#define URL_POST_FAVORITE_FOLDER_P_2            @"?favorite_folder="
#define NOTE_RAITING_STRING                     @"post=%@&vote=%@"
#define NOTE_LOGINING_STRING                    @"login=%@&password=%@"
#define USER_FOLDERS_LIST                       @"profile/get_favorite_folders"

@implementation ConnectionManager
@synthesize userID;

+ (id)sharedManager {
    static ConnectionManager *sharedManager = nil;
    @synchronized(self) {
        if (sharedManager == nil)
            sharedManager = [[self alloc] init];
    }
    return sharedManager;
}

-(instancetype)init{
    NSLog(@"%s",__PRETTY_FUNCTION__);
    self = [super init];
    postsArray = [[NSMutableArray alloc]init];
    

    return self;
}

-(NSURLSession*)currentSession{
    return [NSURLSession sharedSession];
}

-(void)authorizedUser{
    
}

#pragma-mark private method


-(void)newURLDataTaskWithRequest:(NSURLRequest*) request andCompletionHandler:(void (^)(NSDictionary*))completionBlock{
    NSLog(@"request is:%@",request);
    if (request){
        NSURLSessionDataTask *media = [self.currentSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            if (completionBlock)
                completionBlock(dict);
        }];
        [media resume];
    }
}

#pragma mark public methods

-(NSArray *)getPostsArray{
    return postsArray;
}

-(NSArray *)getUserFoldersArray{
    if (!self.userFoldersArray)
        [self getUserFoldersList];
    return self.userFoldersArray;
}

-(void)loginUserWithName:(NSString*)nick andPassword:(NSString*)password{
    NSString *urlString = [NSString stringWithFormat:@"%@%@",URL_ROOT_PART,URL_POST_LOGINIG];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    NSString *noteDataString = [NSString stringWithFormat:NOTE_LOGINING_STRING,nick,password];
    request.HTTPBody = [noteDataString dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPMethod:@"POST"];
    [self newURLDataTaskWithRequest:request andCompletionHandler:^(NSDictionary *dict){[self parsingUserIDFromDictionary:dict];}];
}

-(void)loginUserWithToken:(NSString*)token{
    NSURL *url = [NSURL URLWithString:URL_POST_LOGINIG];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    request.HTTPBody = [token dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPMethod:@"POST"];
    [self newURLDataTaskWithRequest:request andCompletionHandler:^(NSDictionary *dict){[self parsingUserIDFromDictionary:dict];}];
}


-(void)downloadingPathesForFavoritesWithPageNumber:(int) number andFavoriteFolderName:(NSString*)favoriteFolderName{
    NSString *numberString = [NSString stringWithFormat:@"%d",number];
    NSString *urlRequest = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@",
                            URL_ROOT_PART,
                            URL_PICTURES_VIEW_POSTS_PART, numberString,
                            URL_LANG_PART,
                            URL_FAVORITE_BY_PART, self.userID,
                            URL_FAVORITES_IMAGES_FROM_FOLDER_PART, [favoriteFolderName isEqualToString:@"showAll"]? @"": favoriteFolderName];
    NSURL *url = [NSURL URLWithString:urlRequest];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    [self newURLDataTaskWithRequest:request andCompletionHandler:^(NSDictionary *dict){[self parsingFirstDetailsForFavoritesImagesFromDictionary:dict];}];
}


-(void)downloadingPathesForImagesFromSearchDictionary:(NSDictionary*)optionsDictionary{
    

    
    NSString *urlRequest = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@%@%@%@",
                            URL_ROOT_PART,
                            URL_PICTURES_VIEW_POSTS_PART, [optionsDictionary valueForKey:@"page_number"],
                            URL_TAG_PATHES_P_2, [optionsDictionary valueForKey:@"search_tag"],
                            URL_RES_X_PART, [optionsDictionary valueForKey:@"width"],
                            URL_RES_Y_PART,[optionsDictionary valueForKey:@"height"],
                            URL_ORDER_BY_PART,[optionsDictionary valueForKey:@"how_sorting"],
                            URL_DATE_PART,[optionsDictionary valueForKey:@"how_recent"]];
    NSURL *url = [NSURL URLWithString:urlRequest];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    [self newURLDataTaskWithRequest:request andCompletionHandler:^(NSDictionary *dict){[self parsingFirstDetailsForSearchTagImagesFromDictionary:dict];}];
                            
}

-(void)downloadingPathesForImagesFromSearchTag:(NSString*)searchTag forPageWithNumber:(int) pageNumber withHeight:(NSString*)height andWeight:(NSString*) weight withOrderBY:(NSString*) orderBy andRecentBy:(NSString*) recentBy{
    NSString *numberString = [NSString stringWithFormat:@"%d",pageNumber ? pageNumber : 0];
    NSString *urlRequest = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@%@%@%@",
                            URL_ROOT_PART,
                            URL_PICTURES_VIEW_POSTS_PART, numberString,
                            URL_TAG_PATHES_P_2,searchTag,
                            URL_RES_X_PART,weight,
                            URL_RES_Y_PART,height ? height : @"" ,
                            URL_ORDER_BY_PART,orderBy ? orderBy : @"",
                            URL_DATE_PART,recentBy ? recentBy : @""];
    NSURL *url = [NSURL URLWithString:urlRequest];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    [self newURLDataTaskWithRequest:request andCompletionHandler:^(NSDictionary *dict){[self parsingFirstDetailsForSearchTagImagesFromDictionary:dict];}];
}

-(void)downloadFullInfoForPost:(PostObject*) post{
    NSString *urlRequest = [NSString stringWithFormat:@"%@%@%@%@",
                            URL_ROOT_PART,
                            URL_PICTURES_VIEW_POST_PART, [post.imageID stringValue],
                            URL_LANG_PART];
    NSURL *url = [NSURL URLWithString:urlRequest];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    [self newURLDataTaskWithRequest:request andCompletionHandler:^(NSDictionary *dict){[self parsingFullInfoForImageFromDictionary:dict];}];

}

-(void)getUserFoldersList{
    NSString *urlRequest = [NSString stringWithFormat:@"%@%@", URL_ROOT_PART, USER_FOLDERS_LIST];
    NSURL *url = [NSURL URLWithString:urlRequest];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    [self newURLDataTaskWithRequest:request andCompletionHandler:^(NSDictionary *dict){[self parsingUserFoldersFromDictionary:dict];}];
}

-(BOOL)addImageToFavouritesWithSelectedFolder:(NSString*)selectedFolder{
    NSMutableString *urlRequest = [[NSMutableString alloc]initWithFormat:@"%@%@%@%@%@",URL_ROOT_PART, URL_POST_FAVORITE_FOLDER_P_1,currentPostObject.imageID, URL_POST_FAVORITE_FOLDER_P_2, selectedFolder  ? selectedFolder : @"Null" ];
    NSURL *url = [NSURL URLWithString:urlRequest];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    [self newURLDataTaskWithRequest:request andCompletionHandler:^(NSDictionary *dict){[self.firstInfoDelegate postDidAddedToFolder:dict];}];
    return !currentPostObject.isFavorite;    
}

-(BOOL)voteFromImage{
    NSString *raiting = currentPostObject.isVoted? @"0" : @"9";
    NSString *urlString = [NSString stringWithFormat:@"%@%@", URL_ROOT_PART, URL_POST_SET_RAITING];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    NSString *noteDataString = [NSString stringWithFormat: NOTE_RAITING_STRING,currentPostObject.imageID,raiting];
    request.HTTPBody = [noteDataString dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPMethod:@"POST"];
    [self newURLDataTaskWithRequest:request andCompletionHandler:nil];
    return !currentPostObject.isVoted;         // if request was not respond what will be?????
}

#pragma - mark parsing methods

-(void)parsingFirstDetailsForFavoritesImagesFromDictionary:(NSDictionary *) imageDict{
    [self parsingFirstDetailsForImagesFromDictionary:imageDict];
    [self.firstInfoDelegate firstDetailsDidParsingForFavorites];
}

-(void)parsingFirstDetailsForSearchTagImagesFromDictionary:(NSDictionary *) imageDict{
    [self parsingFirstDetailsForImagesFromDictionary:imageDict];
    [self.firstInfoDelegate firstDetailsDidParsingForSearchingTag];
    
}
-(void)parsingFirstDetailsForImagesFromDictionary:(NSDictionary*) imageDict{
    postsArray = [[NSMutableArray alloc] init];
    for (NSDictionary *post in [imageDict valueForKey:@"posts"]){
        PostObject *object = [[PostObject alloc]init];
        object.bigPreviewPath = post[@"big_preview"];
        object.mediumPreviewPath = post[@"medium_preview"];
        object.smallPreviewPath = post[@"small_preview"];
        object.downloadCount = post[@"downloads_count"];
        object.imageWidth = post[@"width"];
        object.imageHeight = post[@"height"];
        object.imageID = post[@"id"];
        [postsArray addObject:object];
    }
}

-(void)parsingFullInfoForImageFromDictionary:(NSDictionary*) imageDict{
    postsArray = [[NSMutableArray alloc] init];
    NSLog(@"Image JSOn :%@", imageDict);
    PostObject *object = [[PostObject alloc]init];

    object.bigPreviewPath = imageDict[@"big_preview"];
    object.mediumPreviewPath = imageDict[@"medium_preview"];
    object.smallPreviewPath = imageDict[@"small_preview"];
    object.downloadCount = imageDict[@"downloads_count"];
    object.imageWidth = imageDict[@"width"];
    object.imageHeight = imageDict[@"height"];
    object.imageID = imageDict[@"id"];
    
    object.fullSizeImagePath = imageDict[@"file_url"];
    object.userName = imageDict[@"user_name"];
    object.isVoted = [imageDict[@"star_it"]intValue];
    object.isFavorite = [imageDict[@"is_favorites"]boolValue];
    object.imageTags = [imageDict valueForKey:@"tags"];
    object.favoriteFolderName = imageDict[@"favorite_folder"];
    object.userAvatarPath = imageDict[@"user_avatar"];
    [postsArray addObject:object];
    currentPostObject = object;
    [self.imageDetailsDelegate imageDetailsDidParsingWithPostObject:object];
}

-(void)parsingUserIDFromDictionary:(NSDictionary *)ids{
    self.userID = nil;
    self.userID = ids[@"user_id"];
    if (self.userID){
        [self downloadingPathesForFavoritesWithPageNumber:0 andFavoriteFolderName:@"showAll"];
        [self getUserFoldersList];
    }
    [self.loginDelegate userDidLogin:(self.userID != 0)];
}

-(void)parsingUserFoldersFromDictionary:(NSDictionary *)folders{
    NSMutableArray *returnFolders = [[NSMutableArray alloc] init];
    for (NSString *folderName in folders) {
        [returnFolders addObject:folderName];
    }
    self.userFoldersArray = returnFolders;
    [self.loginDelegate userFoldersDidParsing:returnFolders];
}

#pragma mark - NSURLSession delegate methods ????

-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite
{
    NSLog(@"Here we go");
}

-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didResumeAtOffset:(int64_t)fileOffset expectedTotalBytes:(int64_t)expectedTotalBytes
{
    NSLog(@"Here we go");
}

-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location
{
    NSLog(@"Here we go");
}

- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential *credential))completionHandler
{
    if (challenge.previousFailureCount == 0)
    {
        NSLog(@"HERE");
    }
    else
    {
        // handle the fact that the previous attempt failed
        NSLog(@"%s: challenge.error = %@", __FUNCTION__, challenge.error);
        completionHandler(NSURLSessionAuthChallengeCancelAuthenticationChallenge, nil);
    }
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task
didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge
 completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential *credential))completionHandler{
    if (challenge.previousFailureCount == 0)
    {
        NSLog(@"did recive challenge");
    }
    else
    {
        NSLog(@"%s; challenge.error = %@", __FUNCTION__, challenge.error);
        completionHandler(NSURLSessionAuthChallengeCancelAuthenticationChallenge, nil);
    }

}

@end

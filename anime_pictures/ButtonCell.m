//
//  ButtonCell.m
//  anime_pictures
//
//  Created by iButs on 01.10.14.
//  Copyright (c) 2014 anime_pictures_net. All rights reserved.
//

#import "ButtonCell.h"
#import "UIButton+StandardStyles.h"
#import "UserInterfaceConstants.h"

@implementation ButtonCell
@synthesize button = _button;

-(UIButton *)button{
    if (!_button){
        _button = [[UIButton alloc]init];
        _button.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _button;
}

-(ButtonCell*)initWithButtonTitle:(NSString*) title andTarget:(SEL) target{
    self = [super init];
    [self addSubview:_button];
    [self addConstraintsForButton];
    [_button addTarget:self action:target forControlEvents:UIControlEventTouchDown];
    return self;
}

-(void)addConstraintsForButton{
    NSLayoutConstraint *width = [NSLayoutConstraint constraintWithItem:self.button attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeWidth multiplier:1.0 constant:-HORIZONTAL_PADDING];
    NSLayoutConstraint *height = [NSLayoutConstraint constraintWithItem:self.button attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:ELEMENTS_HEIGHT];
    NSLayoutConstraint *centerX = [NSLayoutConstraint constraintWithItem:self.button attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0];
    NSLayoutConstraint *bottom = [NSLayoutConstraint constraintWithItem:self.button attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-5];
    [self addConstraints:@[width, height, centerX, bottom]];
}


@end

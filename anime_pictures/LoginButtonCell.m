//
//  LoginButtonCell.m
//  anime_pictures
//
//  Created by iButs on 30.09.14.
//  Copyright (c) 2014 anime_pictures_net. All rights reserved.
//

#import "LoginButtonCell.h"
#import "UIButton+StandardStyles.h"
#import "UserInterfaceConstants.h"

@implementation LoginButtonCell
@synthesize loginButton = _loginButton;

-(instancetype)init{
    self = [super init];
    [self addSubview:self.loginButton];
    [self addConstraintsForLoginButton];
    return self;
}

-(UIButton *)loginButton{
    if (!_loginButton){
        _loginButton = [[UIButton alloc]initWithFrame:CGRectNull];
        [_loginButton setStandardStylesForButton];
        _loginButton.translatesAutoresizingMaskIntoConstraints = NO;
        [_loginButton setTitle:@"Login" forState:UIControlStateNormal];
    }
    return _loginButton;
}

-(void)addConstraintsForLoginButton{
    NSLayoutConstraint *width = [NSLayoutConstraint constraintWithItem:self.loginButton attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeWidth multiplier:1.0 constant:-HORIZONTAL_PADDING];
    NSLayoutConstraint *height = [NSLayoutConstraint constraintWithItem:self.loginButton attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:ELEMENTS_HEIGHT];
    NSLayoutConstraint *centerX = [NSLayoutConstraint constraintWithItem:self.loginButton attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0];
    NSLayoutConstraint *bottom = [NSLayoutConstraint constraintWithItem:self.loginButton attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-5];
    [self addConstraints:@[width, height, centerX, bottom]];
}

@end

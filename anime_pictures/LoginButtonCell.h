//
//  LoginButtonCell.h
//  anime_pictures
//
//  Created by iButs on 30.09.14.
//  Copyright (c) 2014 anime_pictures_net. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginButtonCell : UITableViewCell
@property (strong, nonatomic) UIButton *loginButton;
@end

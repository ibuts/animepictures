//
//  ButtonCell.h
//  anime_pictures
//
//  Created by iButs on 01.10.14.
//  Copyright (c) 2014 anime_pictures_net. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ButtonCell : UIButton
@property (strong, nonatomic) UIButton *button;
@end

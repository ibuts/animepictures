//
//  SearchButtonCell.h
//  anime_pictures
//
//  Created by iButs on 29.09.14.
//  Copyright (c) 2014 anime_pictures_net. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchButtonCell : UITableViewCell
@property (strong, nonatomic) UIButton *searchButton;
@end

//
//  SearchBarCell.m
//  anime_pictures
//
//  Created by iButs on 29.09.14.
//  Copyright (c) 2014 anime_pictures_net. All rights reserved.
//

#import "SearchBarCell.h"

@implementation SearchBarCell
@synthesize searchBar = _searchBar;

-(instancetype)init{
    self = [super init];
    [self addSubview:self.searchBar];
    [self addConstraintsForSearchBar];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    return self;
}


-(UISearchBar *) searchBar{
    if (!_searchBar){
        _searchBar = [[UISearchBar alloc]initWithFrame:CGRectNull];
        _searchBar.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _searchBar;
}


-(void)addConstraintsForSearchBar{
    NSLayoutConstraint *width = [NSLayoutConstraint constraintWithItem:self.searchBar attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0.0];
    NSLayoutConstraint *height = [NSLayoutConstraint constraintWithItem:self.searchBar attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:ELEMENTS_HEIGHT];
    NSLayoutConstraint *centerX = [NSLayoutConstraint constraintWithItem:self.searchBar attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0];
    NSLayoutConstraint *bottom = [NSLayoutConstraint constraintWithItem:self.searchBar attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0];
    [self addConstraints:@[width, height, centerX, bottom]];
}


@end

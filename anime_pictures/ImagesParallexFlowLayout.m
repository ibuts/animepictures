//
//  ImagesParallexFlowLayout.m
//  anime_pictures
//
//  Created by iButs on 01.10.14.
//  Copyright (c) 2014 anime_pictures_net. All rights reserved.
//

#import "ImagesParallexFlowLayout.h"
#import "ImagesParallaxLayoutAttributes.h"
static const CGFloat MaxParallaxOffset = 30.0;

@implementation ImagesParallexFlowLayout

+ (Class)layoutAttributesClass
{
    return [ImagesParallaxLayoutAttributes class];
}

- (CGFloat)maxParallaxOffset
{
    return MaxParallaxOffset;
}

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect
{
    NSArray *layoutAttributesArray =
    [super layoutAttributesForElementsInRect:rect];
    for (ImagesParallaxLayoutAttributes *layoutAttributes in layoutAttributesArray) {
        if (layoutAttributes.representedElementCategory ==
            UICollectionElementCategoryCell)
        {
            layoutAttributes.parallaxOffset =
            [self parallaxOffsetForLayoutAttributes:layoutAttributes];
        }
    }
    return layoutAttributesArray;
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ImagesParallaxLayoutAttributes *layoutAttributes = (ImagesParallaxLayoutAttributes *)
    [super layoutAttributesForItemAtIndexPath:indexPath];
    layoutAttributes.parallaxOffset =
    [self parallaxOffsetForLayoutAttributes:layoutAttributes];
    return layoutAttributes;
}

- (CGPoint)parallaxOffsetForLayoutAttributes:(ImagesParallaxLayoutAttributes *)layoutAttributes
{
    NSParameterAssert(layoutAttributes != nil);
    NSParameterAssert([layoutAttributes isKindOfClass:[ImagesParallaxLayoutAttributes class]]);
    
    CGRect bounds = self.collectionView.bounds;
    CGPoint boundsCenter = CGPointMake(CGRectGetMidX(bounds),
                                       CGRectGetMidY(bounds));
    CGPoint cellCenter = layoutAttributes.center;
    CGPoint offsetFromCenter = CGPointMake(boundsCenter.x - cellCenter.x,
                                           boundsCenter.y - cellCenter.y);
    
    CGSize cellSize = layoutAttributes.size;
    CGFloat maxVerticalOffsetWhereCellIsStillVisible =
    (bounds.size.height / 2) + (cellSize.height / 2);
    CGFloat scaleFactor = self.maxParallaxOffset /
    maxVerticalOffsetWhereCellIsStillVisible;
    
    CGPoint parallaxOffset = CGPointMake(0.0, offsetFromCenter.y * scaleFactor);
    
    return parallaxOffset;
}

-(BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds{
    return YES;
}

@end

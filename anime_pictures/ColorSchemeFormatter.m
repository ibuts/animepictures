//
//  ColorSchemeFormatter.m
//  anime_pictures
//
//  Created by iButs on 01.10.14.
//  Copyright (c) 2014 anime_pictures_net. All rights reserved.
//

#import "ColorSchemeFormatter.h"
#import "UserInterfaceConstants.h"

@interface ColorSchemeFormatter()
@property (strong, nonatomic) UIColor *normalStateBackgroundColor;
@property (strong, nonatomic) UIColor *highlightedStateBackgroundColor;
@end

@implementation ColorSchemeFormatter
@synthesize currentColorsDictionary = _currentColorsDictionary;
@synthesize normalStateBackgroundColor = _normalStateBackgroundColor;
@synthesize highlightedStateBackgroundColor = _higlightedStateBackgroundColor;

-(UIColor *)normalStateBackgroundColor{
    if (!_normalStateBackgroundColor){
        _normalStateBackgroundColor = IOS7_BLUE_COLOR;
    }
    return _normalStateBackgroundColor;
}

-(UIColor *)highlightedStateBackgroundColor{
    if (!_higlightedStateBackgroundColor){
        _higlightedStateBackgroundColor = IOS7_BLUE_COLOR_OPACITY;
    }
    return _higlightedStateBackgroundColor;
}

-(NSMutableDictionary *)currentColorsDictionary{
    if (!_currentColorsDictionary){
        _currentColorsDictionary = [[NSMutableDictionary alloc]initWithObjectsAndKeys:self.normalStateBackgroundColor,@"normalStateColor", self.highlightedStateBackgroundColor ,@"highlightedStateColor",nil];
    }
    return _currentColorsDictionary;
}

+ (id)sharedManager {
    static ColorSchemeFormatter *sharedManager = nil;
    @synchronized(self) {
        if (sharedManager == nil)
            sharedManager = [[self alloc] init];
    }
    return sharedManager;
}

-(void)setCurrentColorScheme:(colorSchemeName)name{
    if (name == standardScheme){
        self.normalStateBackgroundColor = IOS7_BLUE_COLOR;
        self.highlightedStateBackgroundColor = [IOS7_BLUE_COLOR colorWithAlphaComponent:0.2];
    }else if (name == blackScheme){
        self.normalStateBackgroundColor = BLACK_COLOR;
        self.highlightedStateBackgroundColor = [BLACK_COLOR colorWithAlphaComponent:0.2];
    }
}

@end


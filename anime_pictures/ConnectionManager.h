//
//  ConnectionManager.h
//  Anime-pictures
//
//  Created by iButs on 10.04.14.
//  Copyright (c) 2014 iButs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "PostObject.h"
#include <AvailabilityMacros.h>

@protocol loginUserDelegate;
@protocol parsingFirstInfoDelegate;
@protocol parsingFullInfoDelegate;

@interface ConnectionManager : NSObject<NSURLSessionDelegate,NSURLSessionDataDelegate,NSURLSessionDownloadDelegate, NSURLSessionTaskDelegate>
{
    NSMutableArray * postsArray;
    PostObject *currentPostObject;
    
}

@property (nonatomic, weak)id<loginUserDelegate>loginDelegate;
@property (nonatomic, weak)id<parsingFirstInfoDelegate>firstInfoDelegate;
@property (nonatomic, weak)id<parsingFullInfoDelegate>imageDetailsDelegate;
@property (strong, nonatomic) NSString *userID;
@property (strong, nonatomic) NSArray *userFoldersArray;
@property (strong, nonatomic) NSURLSession *currentSession;
@property (strong, nonatomic) NSURLSessionConfiguration *sessionConfiguration;


+ (id)sharedManager;
-(instancetype)init;

-(void)loginUserWithName:(NSString*)nick andPassword:(NSString*)password;
-(void)loginUserWithToken:(NSString*)token;
-(void)downloadingPathesForFavoritesWithPageNumber:(int) number andFavoriteFolderName:(NSString*)favoriteFolderName;
-(void)downloadingPathesForImagesFromSearchTag:(NSString*)searchTag forPageWithNumber:(int) pageNumber withHeight:(NSString*)height andWeight:(NSString*) weight withOrderBY:(NSString*) orderBy andRecentBy:(NSString*) recentBy DEPRECATED_ATTRIBUTE;
-(void)downloadingPathesForImagesFromSearchDictionary:(NSDictionary*)optinonsDictionary;
-(NSArray *)getPostsArray;
-(BOOL)addImageToFavouritesWithSelectedFolder:(NSString*)selectedFolder;
-(BOOL)voteFromImage;
-(void)downloadFullInfoForPost:(PostObject*) post;
-(void)getUserFoldersList;
-(NSArray *)getUserFoldersArray;


@end


#pragma - mark protocols

@protocol loginUserDelegate<NSObject>
-(void)userDidLogin:(BOOL)idIsExist;
@required
-(void)userFoldersDidParsing:(NSArray*)foldersArray;

@end

@protocol parsingFirstInfoDelegate<NSObject>
-(void) postDidAddedToFolder:(NSDictionary *)dict;
@required
-(void)firstDetailsDidParsingForSearchingTag;
-(void)firstDetailsDidParsingForFavorites;
@end

@protocol parsingFullInfoDelegate<NSObject>

@required
-(void)imageDetailsDidParsingWithPostObject:(PostObject*)post;
@end

//
//  BTSSearchView.m
//  anime_pictures
//
//  Created by iButs on 25.09.14.
//  Copyright (c) 2014 anime_pictures_net. All rights reserved.
//


#import "SearchView.h"
#import "UIButton+StandardStyles.h"
#import "UITextField+StandardStyles.h"

@interface SearchView()
@property (strong, nonatomic) UILabel *widthLabel;
@property (strong, nonatomic) UILabel *heightLabel;
@property (strong, nonatomic) UILabel *sortingsLabel;
@property (strong, nonatomic) UILabel *recentLabel;
@end

@implementation SearchView
@synthesize howRecentControl = _howRecentControl;
@synthesize howSortingControl = _howSortingControl;
@synthesize searchBar = _searchBar;
@synthesize sortingsLabel = _sortingsLabel;
@synthesize recentLabel = _recentLabel;
@synthesize widthField = _widthField;
@synthesize heightField = _heightField;
@synthesize widthLabel = _widthLabel;
@synthesize heightLabel = _heightLabel;
@synthesize autosizeButton = _autosizeButton;
@synthesize searchButton = _searchButton;

#pragma mark - initialization BTSSearchView methods

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    [self addSubviews];
    [self addConstraints];
    return self;
}

-(instancetype)init{
    return [self initWithFrame:CGRectNull];
}

-(void)addSubviews{
    [self addSubview:self.sortingsLabel];
    [self addSubview:self.howSortingControl];
    [self addSubview:self.recentLabel];
    [self addSubview:self.howRecentControl];
    [self addSubview:self.widthLabel];
    [self addSubview:self.heightLabel];
    [self addSubview:self.widthField];
    [self addSubview:self.heightField];
    [self addSubview:self.autosizeButton];
    [self.autosizeButton setStandardStylesForButton];
    [self.searchButton setStandardStylesForButton];
}

-(void)addConstraints{
    [self addConstraintsForHowRecentLabel];
    [self addConstraintsForHowRecentControl];
    [self addConstraintsForHowSortingsLabel];
    [self addConstraintsForHowSortingControl];
    [self addConstraintsForWidthLabel];
    [self addConstraintsForWidthField];
    [self addConstraintsForHeightLabel];
    [self addConstraintsForHeightField];
    [self addConstraintsForAutosizeButton];
}

#pragma mark - Initialization of objects

-(UISearchBar *) searchBar{
    if (!_searchBar){
        _searchBar = [[UISearchBar alloc]initWithFrame:CGRectNull];
        _searchBar.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _searchBar;
}

-(UILabel *)recentLabel{
    if (!_recentLabel){
        _recentLabel = [[UILabel alloc]initWithFrame:CGRectNull];
        _recentLabel.text = @"How recent:";
        _recentLabel.textAlignment = NSTextAlignmentLeft;
        _recentLabel.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _recentLabel;
}

-(UILabel *)sortingsLabel{
    if (!_sortingsLabel){
        _sortingsLabel = [[UILabel alloc]initWithFrame:CGRectNull];
        _sortingsLabel.text = @"How to sort:";
        _sortingsLabel.textAlignment = NSTextAlignmentLeft;
        _sortingsLabel.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _sortingsLabel;
}

-(UISegmentedControl *)howRecentControl{
    if (!_howRecentControl){
        _howRecentControl = [[UISegmentedControl alloc]initWithItems:@[@"Any", @"Week", @"Mounth", @"Day"]];
        _howRecentControl.selectedSegmentIndex = 0;
        _howRecentControl.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _howRecentControl;
}

-(UISegmentedControl *)howSortingControl{
    if (!_howSortingControl){
        _howSortingControl = [[UISegmentedControl alloc]initWithItems:@[@"Date", @"Raiting", @"Views", @"Size"]];
        _howSortingControl.selectedSegmentIndex = 0;
        _howSortingControl.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _howSortingControl;
}

-(UITextField *)widthField{
    if (!_widthField){
        _widthField = [[UITextField alloc]initWithFrame:CGRectNull];
        _widthField.translatesAutoresizingMaskIntoConstraints = NO;
        [_widthField setStandardStyles];
    }
    return _widthField;
}

-(UITextField *)heightField{
    if (!_heightField){
        _heightField = [[UITextField alloc]initWithFrame:CGRectNull];
        _heightField.translatesAutoresizingMaskIntoConstraints = NO;
        [_heightField setStandardStyles];
    }
    return _heightField;
}

-(UILabel *)widthLabel{
    if (!_widthLabel){
        _widthLabel = [[UILabel alloc]initWithFrame:CGRectNull];
        _widthLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _widthLabel.text = @"Width:";
    }
    return _widthLabel;
}

-(UILabel *)heightLabel{
    if (!_heightLabel){
        _heightLabel = [[UILabel alloc]initWithFrame:CGRectNull];
        _heightLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _heightLabel.text = @"Height:";
    }
    return _heightLabel;
}

-(UIButton *)autosizeButton{
    if (!_autosizeButton){
        _autosizeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_autosizeButton setTitle:@"Autosizing" forState:UIControlStateNormal];
        _autosizeButton.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _autosizeButton;
}

-(UIButton *)searchButton{
    if (!_searchButton) {
        _searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_searchButton setTitle:@"Search" forState: UIControlStateNormal];
        _searchButton.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _searchButton;
}


#pragma mark - Creating constraints

-(void)addConstraintsForHowRecentLabel{
    NSLayoutConstraint *width = [NSLayoutConstraint constraintWithItem:self.recentLabel attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeWidth multiplier:1.0 constant:-HORIZONTAL_PADDING*2];
    NSLayoutConstraint *height = [NSLayoutConstraint constraintWithItem:self.recentLabel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:ELEMENTS_HEIGHT/2];
    NSLayoutConstraint *left = [NSLayoutConstraint constraintWithItem:self.recentLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1.0 constant:HORIZONTAL_PADDING];
    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:self.recentLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1.0 constant:VERTICAL_PADDING];
    [self addConstraints:@[width, height, left, top]];
}

-(void)addConstraintsForHowRecentControl{
    NSLayoutConstraint *width = [NSLayoutConstraint constraintWithItem:self.howRecentControl attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeWidth multiplier:1.0 constant:-HORIZONTAL_PADDING];
    NSLayoutConstraint *height = [NSLayoutConstraint constraintWithItem:self.howRecentControl attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:ELEMENTS_HEIGHT];
    NSLayoutConstraint *centerX = [NSLayoutConstraint constraintWithItem:self.howRecentControl attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0];
    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:self.howRecentControl attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.recentLabel attribute:NSLayoutAttributeBottom multiplier:1.0 constant:VERTICAL_PADDING];
    [self addConstraints:@[width, height, centerX, top]];
}

-(void)addConstraintsForHowSortingsLabel{
    NSLayoutConstraint *width = [NSLayoutConstraint constraintWithItem:self.sortingsLabel attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeWidth multiplier:1.0 constant:-HORIZONTAL_PADDING*2];
    NSLayoutConstraint *height = [NSLayoutConstraint constraintWithItem:self.sortingsLabel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:ELEMENTS_HEIGHT/2];
    NSLayoutConstraint *left = [NSLayoutConstraint constraintWithItem:self.sortingsLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1.0 constant:HORIZONTAL_PADDING];
    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:self.sortingsLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.howRecentControl attribute:NSLayoutAttributeBottom multiplier:1.0 constant:VERTICAL_PADDING];
    [self addConstraints:@[width, height, left, top]];
}

-(void)addConstraintsForHowSortingControl{
    NSLayoutConstraint *width = [NSLayoutConstraint constraintWithItem:self.howSortingControl attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeWidth multiplier:1.0 constant:-HORIZONTAL_PADDING];
    NSLayoutConstraint *height = [NSLayoutConstraint constraintWithItem:self.howSortingControl attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:ELEMENTS_HEIGHT];
    NSLayoutConstraint *centerX = [NSLayoutConstraint constraintWithItem:self.howSortingControl attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0];
    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:self.howSortingControl attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.sortingsLabel attribute:NSLayoutAttributeBottom multiplier:1.0 constant:VERTICAL_PADDING];
    [self addConstraints:@[width, height, centerX, top]];
}

-(void)addConstraintsForWidthLabel{
    NSLayoutConstraint *width = [NSLayoutConstraint constraintWithItem:self.widthLabel attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:ELEMENTS_HEIGHT+10];
    NSLayoutConstraint *height = [NSLayoutConstraint constraintWithItem:self.widthLabel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:ELEMENTS_HEIGHT];
    NSLayoutConstraint *left = [NSLayoutConstraint constraintWithItem:self.widthLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1.0 constant:HORIZONTAL_PADDING];
    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:self.widthLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.howSortingControl attribute:NSLayoutAttributeBottom multiplier:1.0 constant:VERTICAL_PADDING];
    [self addConstraints:@[width, height, left, top]];
}

-(void)addConstraintsForWidthField{
    NSLayoutConstraint *width = [NSLayoutConstraint constraintWithItem:self.widthField attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:ELEMENTS_HEIGHT+20];
    NSLayoutConstraint *height = [NSLayoutConstraint constraintWithItem:self.widthField attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:ELEMENTS_HEIGHT];
    NSLayoutConstraint *left = [NSLayoutConstraint constraintWithItem:self.widthField attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.widthLabel attribute:NSLayoutAttributeRight multiplier:1.0 constant:HORIZONTAL_PADDING];
    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:self.widthField attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.howSortingControl attribute:NSLayoutAttributeBottom multiplier:1.0 constant:VERTICAL_PADDING];
    [self addConstraints:@[width, height, left, top]];
}

-(void)addConstraintsForHeightLabel{
    NSLayoutConstraint *width = [NSLayoutConstraint constraintWithItem:self.heightLabel attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:ELEMENTS_HEIGHT+10];
    NSLayoutConstraint *height = [NSLayoutConstraint constraintWithItem:self.heightLabel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:ELEMENTS_HEIGHT];
    NSLayoutConstraint *left = [NSLayoutConstraint constraintWithItem:self.heightLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.widthField attribute:NSLayoutAttributeRight multiplier:1.0 constant:HORIZONTAL_PADDING];
    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:self.heightLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.howSortingControl attribute:NSLayoutAttributeBottom multiplier:1.0 constant:VERTICAL_PADDING];
    [self addConstraints:@[width, height, left, top]];
}

-(void)addConstraintsForHeightField{
    NSLayoutConstraint *width = [NSLayoutConstraint constraintWithItem:self.heightField attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:ELEMENTS_HEIGHT+20];
    NSLayoutConstraint *height = [NSLayoutConstraint constraintWithItem:self.heightField attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:ELEMENTS_HEIGHT];
    NSLayoutConstraint *left = [NSLayoutConstraint constraintWithItem:self.heightField attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.heightLabel attribute:NSLayoutAttributeRight multiplier:1.0 constant:HORIZONTAL_PADDING];
    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:self.heightField attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.howSortingControl attribute:NSLayoutAttributeBottom multiplier:1.0 constant:VERTICAL_PADDING];
    [self addConstraints:@[width, height, left, top]];
}

-(void)addConstraintsForAutosizeButton{
    NSLayoutConstraint *width = [NSLayoutConstraint constraintWithItem:self.autosizeButton attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeWidth multiplier:1.0 constant:-HORIZONTAL_PADDING];
    NSLayoutConstraint *height = [NSLayoutConstraint constraintWithItem:self.autosizeButton attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:ELEMENTS_HEIGHT];
    NSLayoutConstraint *centerX = [NSLayoutConstraint constraintWithItem:self.autosizeButton attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0];
    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:self.autosizeButton attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.heightField attribute:NSLayoutAttributeBottom multiplier:1.0 constant:VERTICAL_PADDING];
    [self addConstraints:@[width, height, centerX, top]];
}

@end

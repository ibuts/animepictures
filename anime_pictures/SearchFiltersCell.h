//
//  SearchFiltersCell.h
//  anime_pictures
//
//  Created by iButs on 29.09.14.
//  Copyright (c) 2014 anime_pictures_net. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTSSearchView.h"

@interface SearchFiltersCell : UITableViewCell
@property (strong, nonatomic) BTSSearchView *searchView;
@property (strong, nonatomic) UIView *colorView;
-(SearchFiltersCell*)initWithColorView:(UIView*)colorView;
@end

//
//  ImagesParallaxLayoutAttributes.m
//  anime_pictures
//
//  Created by iButs on 01.10.14.
//  Copyright (c) 2014 anime_pictures_net. All rights reserved.
//

#import "ImagesParallaxLayoutAttributes.h"

@implementation ImagesParallaxLayoutAttributes

- (id)copyWithZone:(NSZone *)zone
{
    ImagesParallaxLayoutAttributes *copy = [super copyWithZone:zone];
    NSAssert([copy isKindOfClass:[self class]], @"copy must have the same class");
    copy.parallaxOffset = self.parallaxOffset;
    return copy;
}

- (BOOL)isEqual:(id)object
{
    if (![object isKindOfClass:[ImagesParallaxLayoutAttributes class]]) {
        return NO;
    }
    
    ImagesParallaxLayoutAttributes *otherObject = object;
    if (!CGPointEqualToPoint(self.parallaxOffset, otherObject.parallaxOffset)) {
        return NO;
    }
    return [super isEqual:otherObject];
}
@end


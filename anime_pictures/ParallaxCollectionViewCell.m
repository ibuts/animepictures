//
//  ParallaxCollectionViewCell.m
//  anime_pictures
//
//  Created by iButs on 01.10.14.
//  Copyright (c) 2014 anime_pictures_net. All rights reserved.
//

#import "ParallaxCollectionViewCell.h"
#import "ImagesParallaxLayoutAttributes.h"

@implementation ParallaxCollectionViewCell
@synthesize imageViewCenterYConstraint = _imageViewCenterYConstraint;
@synthesize imageViewCenterXConstraint = _imageViewCenterXConstraint;

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    
    if (self) {

        UIImageView *imageView = [[UIImageView alloc] initWithFrame:self.contentView.bounds];
        imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;

        [self.contentView addSubview:imageView];
        self.imageView = imageView;
        self.imageView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addConstraint:self.imageViewCenterYConstraint];
        [self.contentView addConstraint:self.imageViewCenterXConstraint];
    }
    return self;
}

-(NSLayoutConstraint *)imageViewCenterYConstraint{
    if (!_imageViewCenterYConstraint){
        _imageViewCenterYConstraint = [NSLayoutConstraint constraintWithItem:self.imageView
                                         attribute:NSLayoutAttributeCenterY
                                         relatedBy:NSLayoutRelationEqual
                                            toItem:self.contentView
                                         attribute:NSLayoutAttributeCenterY
                                        multiplier:1
                                          constant:0];
    }
    return _imageViewCenterYConstraint;
}

-(NSLayoutConstraint *) imageViewCenterXConstraint{
    if (!_imageViewCenterXConstraint){
        _imageViewCenterXConstraint = [NSLayoutConstraint constraintWithItem:self.imageView
                                                                   attribute:NSLayoutAttributeCenterX
                                                                   relatedBy:NSLayoutRelationEqual
                                                                      toItem:self.contentView
                                                                   attribute:NSLayoutAttributeCenterX
                                                                  multiplier:1
                                                                    constant:0];
    }
    return _imageViewCenterXConstraint;
}

- (void)applyLayoutAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes
{
    [super applyLayoutAttributes:layoutAttributes];
//    
//    NSParameterAssert(layoutAttributes != nil);
//    NSParameterAssert([layoutAttributes isKindOfClass:[ImagesParallaxLayoutAttributes class]]);
//    
//    ImagesParallaxLayoutAttributes *parallaxLayoutAttributes =
//    (ImagesParallaxLayoutAttributes *)layoutAttributes;
//    self.imageViewCenterYConstraint.constant = parallaxLayoutAttributes.parallaxOffset.y;
}


@end


//
//  UIButton+StandardStyles.m
//  anime_pictures
//
//  Created by iButs on 28.09.14.
//  Copyright (c) 2014 anime_pictures_net. All rights reserved.
//

#import "UIButton+StandardStyles.h"
#import "UserInterfaceConstants.h"
#import "ColorSchemeFormatter.h"
#import "ColorSchemeFormatter.h"
#import <QuartzCore/QuartzCore.h>

@interface UIButton()
@property (strong, nonatomic) ColorSchemeFormatter *sharedFormatter;
@end

@implementation UIButton (StandardStyles)


-(ColorSchemeFormatter *)sharedFormatter{
    return  [ColorSchemeFormatter sharedManager];
}
 
-(void)setStandardStylesForButton{
    self.layer.borderWidth = 1.0f;
    [self.layer setCornerRadius:8];
    self.layer.masksToBounds = YES;
    self.translatesAutoresizingMaskIntoConstraints = NO;
    [self.titleLabel setFont:[UIFont systemFontOfSize:BUTTONS_FONT_SIZE]];
    
    
    UIColor * highlightedColor =(UIColor*)[[self.sharedFormatter currentColorsDictionary] valueForKey:@"highlightedStateColor"];
    UIColor * normalColor = (UIColor*)[[self.sharedFormatter currentColorsDictionary] valueForKey:@"normalStateColor"];
    
    self.layer.borderColor = [normalColor CGColor];
    [self setBackgroundImage:[self imageWithColor:normalColor] forState:UIControlStateNormal];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self setBackgroundImage:[self imageWithColor:highlightedColor] forState:UIControlStateHighlighted];
    [self setTitleColor:normalColor forState:UIControlStateHighlighted];
}


- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

@end

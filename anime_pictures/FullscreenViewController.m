//
//  FullscreenViewController.m
//  anime_pictures
//
//  Created by iButs on 02.10.14.
//  Copyright (c) 2014 anime_pictures_net. All rights reserved.
//

#import "FullscreenViewController.h"
#import "UserInterfaceConstants.h"

@implementation FullscreenViewController
@synthesize currentPost;
@synthesize shareButton = _shareButton;
@synthesize foldersListSheet = _foldersListSheet;
@synthesize picker = _picker;
@synthesize foldersList = _foldersList;


-(UIPickerView *)picker{
    if (!_picker){
        _picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        _picker.showsSelectionIndicator=YES;
        _picker.delegate = self;
        _picker.dataSource = self;
        _picker.showsSelectionIndicator = YES;
        _picker.translatesAutoresizingMaskIntoConstraints = NO;
    }

    return _picker;
}

-(NSArray *)foldersList{
    if (!_foldersList){
        NSMutableArray *array = [NSMutableArray arrayWithObject:@"default"];
        [array addObjectsFromArray:[self.sharedManager getUserFoldersArray]];
        _foldersList = [NSArray arrayWithArray:array];
    }
    return _foldersList;
}

-(UIAlertController *)foldersListSheet{
    if (!_foldersListSheet){
        _foldersListSheet = [UIAlertController alertControllerWithTitle:@"Folders list" message:@"\n\n\n\n\n\n\n" preferredStyle:UIAlertControllerStyleActionSheet];
        _foldersListSheet.view.bounds = CGRectMake(0, 0, 0, 0);
        [_foldersListSheet.view addSubview:self.picker];
        [_foldersListSheet setModalPresentationStyle:UIModalPresentationPopover];

        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            _foldersListSheet = nil;
        }];

        UIAlertAction *selectAction = [UIAlertAction actionWithTitle:@"select" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self.sharedManager addImageToFavouritesWithSelectedFolder:[self.foldersList objectAtIndex:[self.picker selectedRowInComponent:0]]];
            self.currentPost.isFavorite = YES;
            _foldersListSheet = nil;

        }];

        UIAlertAction *deleteAction = [UIAlertAction actionWithTitle:@"delete" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
            [self.sharedManager addImageToFavouritesWithSelectedFolder:@"Null"];
            self.currentPost.isFavorite = NO;
            _foldersListSheet = nil;
        }];
        [_foldersListSheet addAction:selectAction];
        [_foldersListSheet addAction:cancelAction];
        _foldersListSheet.view.translatesAutoresizingMaskIntoConstraints = NO;
            if (currentPost.isFavorite) [_foldersListSheet addAction:deleteAction];

        NSLayoutConstraint *itsX = [NSLayoutConstraint constraintWithItem:self.picker attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:_foldersListSheet.view attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0];
        NSLayoutConstraint *itsTop = [NSLayoutConstraint constraintWithItem:self.picker attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:_foldersListSheet.view attribute:NSLayoutAttributeTop multiplier:1.0f constant:20.0f];
        NSLayoutConstraint *itsWidth = [NSLayoutConstraint constraintWithItem:self.picker attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:_foldersListSheet.view attribute:NSLayoutAttributeWidth multiplier:1.0f constant:0.0];
        NSLayoutConstraint *itsHeight = [NSLayoutConstraint constraintWithItem:self.picker attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:160.0f];
        [_foldersListSheet.view addConstraints:@[itsX, itsWidth, itsHeight, itsTop]];
        


    }
    UIPopoverPresentationController *popPresenter = [_foldersListSheet popoverPresentationController];
    popPresenter.sourceView = self.shareButton;
    popPresenter.sourceRect = self.shareButton.bounds;
    NSLayoutConstraint *width = [NSLayoutConstraint constraintWithItem:_foldersListSheet.view attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:20.0];
    [_foldersListSheet.view.superview addConstraint:width];

    return _foldersListSheet;
}

-(ConnectionManager *)sharedManager{
    return [ConnectionManager sharedManager];
}

-(UIButton *)shareButton{
    if (!_shareButton){
        _shareButton = [[UIButton alloc]initWithFrame:CGRectNull];
        [_shareButton setTitle:@"Share" forState:UIControlStateNormal];
        [_shareButton addTarget:self action:@selector(shareButtonDidPressed:) forControlEvents:UIControlEventTouchDown];
        [_shareButton setStandardStylesForButton];
        _shareButton.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _shareButton;
}

-(instancetype)initWithPostObject:(PostObject*)postObject{
    self = [super init];
    self.currentPost = postObject;
    self.sharedManager.imageDetailsDelegate = self;
    self.view.frame = [[UIScreen mainScreen] bounds];
    self.postImage = [[UIImageView alloc]initWithFrame:self.view.frame];
    self.postImage.userInteractionEnabled = YES;
    [self.postImage addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapHandler:)]];
    self.postImage.translatesAutoresizingMaskIntoConstraints = NO;
    self.modalPresentationStyle = UIModalPresentationFormSheet;
    self.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self.view addSubview:self.postImage];
    [self.view addSubview:self.shareButton];
    [self addConstraintsForPostImage];
    [self addConstraintsForShareButton];

    [self.sharedManager downloadFullInfoForPost:postObject];


    return self;
}

-(void)tapHandler:(UIGestureRecognizer*)gesture{
    NSURL *url = [NSURL URLWithString:[self.currentPost.fullSizeImagePath stringByReplacingOccurrencesOfString:@" " withString:@"%20"]];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:120.0];
        NSData * data = [NSURLConnection sendSynchronousRequest:request
                                              returningResponse:nil
                                                          error:nil];
    if (data){
            UIImage *image = [UIImage imageWithData:data];
            UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
        }
    });
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if (error != NULL)
    {
        // handle error
        //hide status bar
        //create my custom status bar and show here with message
    }
    else
    {
        // handle ok status
        //hide status bar
        //create my custom status bar and show here with message
    }
}

-(void)imageDetailsDidParsingWithPostObject:(PostObject *)post{
    self.currentPost = post;
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:post.bigPreviewPath]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:20.0];
        NSData * data = [NSURLConnection sendSynchronousRequest:request
                                              returningResponse:nil
                                                          error:nil];
        if (data){
            dispatch_sync(dispatch_get_main_queue(),^{
                UIImage *image = [UIImage imageWithData:data];
                self.postImage.image = image;
                CGRect frame = CGRectMake(0, 0, image.size.width, image.size.height);
                self.postImage.frame = frame;
            });
        }
    });
}

-(void)addConstraintsForPostImage{
    NSLayoutConstraint *centerX = [NSLayoutConstraint constraintWithItem:self.postImage attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0];
    NSLayoutConstraint *centerY = [NSLayoutConstraint constraintWithItem:self.postImage attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0];
    [self.view addConstraints:@[centerX, centerY]];
}

-(void)addConstraintsForShareButton{
    NSLayoutConstraint *left = [NSLayoutConstraint constraintWithItem:self.shareButton attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeft multiplier:1.0 constant:HORIZONTAL_PADDING];
    NSLayoutConstraint *bottom = [NSLayoutConstraint constraintWithItem:self.shareButton attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-VERTICAL_PADDING];
    NSLayoutConstraint *width = [NSLayoutConstraint constraintWithItem:self.shareButton attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:SHARE_BUTTON_WIDTH];
    NSLayoutConstraint *height = [NSLayoutConstraint constraintWithItem:self.shareButton attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:ELEMENTS_HEIGHT];
    [self.view addConstraints:@[left, bottom, width, height]];
    
}

-(void)shareButtonDidPressed:(UIButton*)sender{
    UIViewController *vc = [[UIViewController alloc] init];
    [vc.view addSubview:self.picker];
    vc.view.frame = CGRectMake(0, 0, 200, 400);
    NSLayoutConstraint *itsX = [NSLayoutConstraint constraintWithItem:self.picker attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:vc.view attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0];
    NSLayoutConstraint *itsTop = [NSLayoutConstraint constraintWithItem:self.picker attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:vc.view attribute:NSLayoutAttributeTop multiplier:1.0f constant:0.0f];
    NSLayoutConstraint *itsWidth = [NSLayoutConstraint constraintWithItem:self.picker attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:vc.view attribute:NSLayoutAttributeWidth multiplier:1.0f constant:0.0];
    NSLayoutConstraint *itsHeight = [NSLayoutConstraint constraintWithItem:self.picker attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:160.0f];
    [vc.view addConstraints:@[itsX, itsWidth, itsHeight, itsTop]];
    UIPopoverController *popPresenter = [[UIPopoverController alloc] initWithContentViewController:vc];
    popPresenter.delegate = self;
    popPresenter.popoverContentSize=CGSizeMake(200, 400);
    [popPresenter presentPopoverFromRect:self.shareButton.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
//    popPresenter.delegate = self;
//    popPresenter.adaptivePresentationStyle = UIModalPresentationOverFullScreen;
//    popPresenter.sourceView = self.shareButton;
//    popPresenter.sourceRect = self.shareButton.bounds;
//    [self presentViewController:vc animated:YES completion:nil];
//    [self.sharedManager addImageToFavouritesWithSelectedFolder:@"default"];
}

-(void)doneButtonDidPressed:(UIButton*)button{
    
}



#pragma mark - UIPickerViewDataSource


// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return self.foldersList.count;
}

#pragma mark - UIPickerViewDelegate

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 40.0f;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return [self.foldersList objectAtIndex:row];
}

//- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component {
//    
//}

//- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
//    
//}

- (void)popoverController:(UIPopoverController *)popoverController willRepositionPopoverToRect:(inout CGRect *)rect inView:(inout UIView **)view NS_AVAILABLE_IOS(7_0){
    
}

@end

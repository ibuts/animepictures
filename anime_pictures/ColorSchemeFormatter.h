//
//  ColorSchemeFormatter.h
//  anime_pictures
//
//  Created by iButs on 01.10.14.
//  Copyright (c) 2014 anime_pictures_net. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
typedef enum {
    standardScheme,
    blackScheme,
}colorSchemeName;

@interface ColorSchemeFormatter : NSObject
@property (strong, nonatomic, readonly) NSMutableDictionary *currentColorsDictionary;
@property  colorSchemeName nameOfScheme;

+ (id)sharedManager;

-(void)setCurrentColorScheme:(colorSchemeName)name;

@end

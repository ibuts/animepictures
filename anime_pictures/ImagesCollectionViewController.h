//
//  ImagesCollectionViewController.h
//  anime_pictures
//
//  Created by iButs on 30.09.14.
//  Copyright (c) 2014 anime_pictures_net. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConnectionManager.h"
#import "SearchViewController.h"

@interface ImagesCollectionViewController : UICollectionViewController<parsingFirstInfoDelegate,
                                                                        SearchControllerDelegate,
                                                                        UIScrollViewDelegate,
                                                                        UICollectionViewDelegate,
                                                                        UICollectionViewDataSource>{
    BOOL isMineSource;
}

@property (strong, nonatomic) UICollectionViewFlowLayout *flowLayout;
@property (strong, nonatomic) UICollectionViewCell *collectionCell;
@property (strong, nonatomic) ConnectionManager *connectionManager;
@property (strong, nonatomic) NSMutableArray *parsingPosts;
@property (strong, nonatomic) UIPageControl *pageControl;
@property (strong, nonatomic) NSDictionary *currentRequestDictionary;
@property (getter=isFavorites) BOOL favorites;
@property (strong, nonatomic) NSString *selectedFolderName;
@end

//
//  AuthentificationButtonCell.h
//  anime_pictures
//
//  Created by iButs on 30.09.14.
//  Copyright (c) 2014 anime_pictures_net. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AuthentificationButtonCell : UITableViewCell
@property (strong, nonatomic) UIButton *authentificationButton;
@end

//
//  ImagesCollectionViewController.m
//  anime_pictures
//
//  Created by iButs on 30.09.14.
//  Copyright (c) 2014 anime_pictures_net. All rights reserved.
//
#define PAGING_ENABLED 1
#import "ImagesCollectionViewController.h"
#import "FullscreenViewController.h"
#import "ParallaxCollectionViewCell.h"

@implementation ImagesCollectionViewController
@synthesize flowLayout = _flowLayout;
@synthesize collectionCell = _collectionCell;
@synthesize parsingPosts = _parsingPosts;
@synthesize pageControl = _pageControl;
@synthesize selectedFolderName = _selectedFolderName;

-(NSMutableArray *)parsingPosts{
    if (!_parsingPosts){
        _parsingPosts = [[NSMutableArray alloc]init];
    }
    return _parsingPosts;
}

-(ConnectionManager *)connectionManager{
    return [ConnectionManager sharedManager];
}

-(UICollectionViewFlowLayout *)flowLayout{
    if (!_flowLayout){
        _flowLayout = [[UICollectionViewFlowLayout alloc] init];
        _flowLayout.itemSize = CGSizeMake(150, 150);
        _flowLayout.sectionInset = UIEdgeInsetsMake(5, 2, 5, 2);
        _flowLayout.minimumInteritemSpacing = 2.0f;
        _flowLayout.minimumLineSpacing = 5.0f;
        [_flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    }
    return _flowLayout;
}

-(UIPageControl *)pageControl{
    if (!_pageControl){
        _pageControl = [[UIPageControl alloc]initWithFrame:self.collectionView.frame];
    }
    return _pageControl;
}

-(instancetype)init{
    self = [super initWithCollectionViewLayout:self.flowLayout];
    [self.collectionView setPagingEnabled:PAGING_ENABLED];
    self.connectionManager.firstInfoDelegate = self;
    return self;
}

-(void)viewDidLoad{

    self.pageControl.numberOfPages = floor(self.collectionView.contentSize.height / self.collectionView.frame.size.height) + 1;
    self.pageControl.currentPage = 0;

}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [self.parsingPosts count];
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    [self.collectionView registerClass:[ParallaxCollectionViewCell class] forCellWithReuseIdentifier:@"cvCell"];
    ParallaxCollectionViewCell *cell =[self.collectionView dequeueReusableCellWithReuseIdentifier:@"cvCell" forIndexPath:indexPath];
    PostObject *currentPost = [self.parsingPosts objectAtIndex:indexPath.row];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:currentPost.smallPreviewPath]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:20.0];
        NSData * data = [NSURLConnection sendSynchronousRequest:request
                                              returningResponse:nil
                                                          error:nil];
        if (data){
            dispatch_sync(dispatch_get_main_queue(),^{
                UIImage *image = [UIImage imageWithData:data];
                cell.imageView.image = image;
                cell.imageView.frame = CGRectMake(0, 0, image.size.width, image.size.height);
            });
        }
    });
    return cell;
}



-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:[[FullscreenViewController alloc]initWithPostObject:[self.parsingPosts objectAtIndex:indexPath.row]]];
    navController.modalPresentationStyle = UIModalPresentationFormSheet;
    navController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:navController animated:YES completion:nil];
}

-(void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath{
    
}

#pragma mark - connection manager first details delegate methods

-(void)firstDetailsDidParsingForSearchingTag{
    [self firstDetailsDidParsing];
}

-(void)firstDetailsDidParsingForFavorites{
    [self firstDetailsDidParsing];
}

-(void)firstDetailsDidParsing{
    if (self.pageControl.currentPage == 0)
        self.parsingPosts = nil;
    [self.parsingPosts addObjectsFromArray:[self.connectionManager getPostsArray]];
    CGRect visibleRect = (CGRect){.origin = self.collectionView.contentOffset, .size = self.collectionView.bounds.size};
    CGPoint visiblePoint = CGPointMake(10, CGRectGetMidY(visibleRect));
    NSIndexPath *visibleIndexPath = [self.collectionView indexPathForItemAtPoint:visiblePoint];
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.collectionView reloadData];
    });
    if (visibleIndexPath.row > (UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication]statusBarOrientation])? 30 : 20))
        [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredVertically animated:YES];
}

#pragma mark - connection manager post object manipulation delegate methods

-(void)postDidAddedToFolder:(NSDictionary *)dict{
    int success = (int)[dict valueForKey:@"success"];
    if ((success)&&(self.selectedFolderName.length > 0)){
        [self.connectionManager downloadingPathesForFavoritesWithPageNumber:0 andFavoriteFolderName:self.selectedFolderName];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.collectionView reloadData];
        });
    }
}

#pragma mark - search controller delegate methods

-(void)searchButtonDidPressedForSearchWithDictionary:(NSDictionary *)optionsDictionary{
    self.selectedFolderName = nil;
    [self.connectionManager downloadingPathesForImagesFromSearchDictionary:optionsDictionary];
}

-(void)didSelectedFolderWithName:(NSString *)folderName{
    self.selectedFolderName = folderName;
    [self.connectionManager downloadingPathesForFavoritesWithPageNumber:0 andFavoriteFolderName:self.selectedFolderName];
}

#pragma mark - scroll view delegate methods

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    NSInteger page = (NSInteger)floor(self.collectionView.contentOffset.y / self.collectionView.frame.size.height);
    self.pageControl.currentPage = page;
//    if (page == 3) [self.connectionManager downloadingPathesForFavoritesWithPageNumber:1];

}


@end

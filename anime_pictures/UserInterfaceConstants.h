//
//  UserInterfaceConstants.h
//  anime_pictures
//
//  Created by iButs on 29.09.14.
//  Copyright (c) 2014 anime_pictures_net. All rights reserved.
//

#ifndef anime_pictures_UserInterfaceConstants_h
#define anime_pictures_UserInterfaceConstants_h

//COLORS

#define IOS7_BLUE_COLOR [UIColor colorWithRed:0.0 green:122.0/255.0 blue:1.0 alpha:1.0]
#define IOS7_BLUE_COLOR_OPACITY [IOS7_BLUE_COLOR colorWithAlphaComponent:0.2]

#define BLACK_COLOR [UIColor blackColor]

//SIZES

#define HORIZONTAL_PADDING 20.0f
#define VERTICAL_PADDING 10.0f
#define ELEMENTS_HEIGHT 40.0f
#define BUTTONS_FONT_SIZE 16.0f
#define TOP_BAR_PADDING 20.0f
#define SHARE_BUTTON_WIDTH 200.0f
#endif

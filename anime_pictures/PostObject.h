//
//  PostObject.h
//  anime_pictures
//
//  Created by iButs on 04.10.14.
//  Copyright (c) 2014 anime_pictures_net. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PostObject : NSObject
@property (strong, nonatomic)    NSString *fullSizeImagePath;
@property (strong, nonatomic)    NSString *bigPreviewPath;
@property (strong, nonatomic)    NSString *mediumPreviewPath;
@property (strong, nonatomic)    NSString *smallPreviewPath;
@property (strong, nonatomic)    NSNumber *imageID;
@property (strong, nonatomic)    NSNumber *downloadCount;
@property (strong, nonatomic)    NSDate *pubTime;
@property (strong, nonatomic)    NSNumber *imageWidth;
@property (strong, nonatomic)    NSNumber *imageHeight;
@property (strong, nonatomic)    NSString *userName;
@property (strong, nonatomic)    NSArray *imageTags;
@property (strong, nonatomic)    NSString *favoriteFolderName;
@property (strong, nonatomic)    NSString *userAvatarPath;
@property int isVoted;
@property BOOL isFavorite;
@end

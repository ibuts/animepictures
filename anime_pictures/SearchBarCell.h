//
//  SearchBarCell.h
//  anime_pictures
//
//  Created by iButs on 29.09.14.
//  Copyright (c) 2014 anime_pictures_net. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserInterfaceConstants.h"

@interface SearchBarCell : UITableViewCell
@property (strong, nonatomic) UISearchBar *searchBar;
@end

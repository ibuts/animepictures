//
//  SearchViewController.h
//  anime_pictures
//
//  Created by iButs on 24.09.14.
//  Copyright (c) 2014 anime_pictures_net. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTSSearchView.h"
#import "FCColorPickerViewController.h"
#import "ConnectionManager.h"

@protocol SearchControllerDelegate;

@interface SearchViewController : UIViewController <UITableViewDelegate,
                                                    UITableViewDataSource,
                                                    UISearchControllerDelegate,
                                                    UISearchBarDelegate,
                                                    UITextFieldDelegate,
                                                    loginUserDelegate,
                                                    FCColorPickerViewControllerDelegate>
@property (nonatomic, weak)id<SearchControllerDelegate> delegate;

@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) BTSSearchView *searchView;
@property (strong, nonatomic) FCColorPickerViewController *colorController;
@property (strong, nonatomic) ConnectionManager *connectionManager;

@end

@protocol SearchControllerDelegate<NSObject>
@required
-(void)didSelectedFolderWithName:(NSString *)folderName;
-(void)searchButtonDidPressedForSearchWithDictionary:(NSDictionary *)optionsDictionary;
@end

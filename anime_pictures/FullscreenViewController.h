//
//  FullscreenViewController.h
//  anime_pictures
//
//  Created by iButs on 02.10.14.
//  Copyright (c) 2014 anime_pictures_net. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConnectionManager.h"
#import "UIButton+StandardStyles.h"
#import "PostObject.h"

@interface FullscreenViewController : UIViewController<parsingFullInfoDelegate, UIActionSheetDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UIPopoverControllerDelegate>
@property (strong, nonatomic) ConnectionManager *sharedManager;
@property (strong, nonatomic) PostObject *currentPost;
@property (strong, nonatomic) UIImageView *postImage;
@property (strong, nonatomic) UIButton *shareButton;
@property (strong, nonatomic) UIAlertController *foldersListSheet;
@property (strong, nonatomic) UIPickerView *picker;
@property (strong, nonatomic) NSArray *foldersList;
-(instancetype)initWithPostObject:(PostObject*)postObject;
@end

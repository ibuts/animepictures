//
//  UITextField+StandardStyles.m
//  anime_pictures
//
//  Created by iButs on 28.09.14.
//  Copyright (c) 2014 anime_pictures_net. All rights reserved.
//

#import "UITextField+StandardStyles.h"
#import "UserInterfaceConstants.h"

@implementation UITextField (StandardStyles)

-(void)setStandardStyles{
    self.layer.cornerRadius = 8.0f;
    self.layer.masksToBounds = YES;
    [[self layer] setBorderColor:[IOS7_BLUE_COLOR CGColor]];
    self.layer.borderWidth = 1.0f;
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    self.leftView = paddingView;
    self.leftViewMode = UITextFieldViewModeAlways;
    [self setKeyboardType:UIKeyboardTypePhonePad];
}
@end

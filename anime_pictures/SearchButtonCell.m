//
//  SearchButtonCell.m
//  anime_pictures
//
//  Created by iButs on 29.09.14.
//  Copyright (c) 2014 anime_pictures_net. All rights reserved.
//

#import "SearchButtonCell.h"
#import "UserInterfaceConstants.h"
#import "UIButton+StandardStyles.h"

@implementation SearchButtonCell
@synthesize searchButton = _searchButton;

-(instancetype)init{
    self = [super init];
    [self addSubview:self.searchButton];
    [self addConstraintsForSearchButton];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    return self;
}

-(UIButton *)searchButton{
    if (!_searchButton) {
        _searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_searchButton setTitle:@"Search" forState: UIControlStateNormal];
        _searchButton.translatesAutoresizingMaskIntoConstraints = NO;
        [_searchButton setStandardStylesForButton];
    }
    return _searchButton;
}

-(void)addConstraintsForSearchButton{
    NSLayoutConstraint *width = [NSLayoutConstraint constraintWithItem:self.searchButton attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeWidth multiplier:1.0 constant:-HORIZONTAL_PADDING];
    NSLayoutConstraint *height = [NSLayoutConstraint constraintWithItem:self.searchButton attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:ELEMENTS_HEIGHT];
    NSLayoutConstraint *centerX = [NSLayoutConstraint constraintWithItem:self.searchButton attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0];
    NSLayoutConstraint *bottom = [NSLayoutConstraint constraintWithItem:self.searchButton attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-5];
    [self addConstraints:@[width, height, centerX, bottom]];
}

@end

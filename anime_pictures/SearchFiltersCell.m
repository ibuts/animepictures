//
//  SearchFiltersCell.m
//  anime_pictures
//
//  Created by iButs on 29.09.14.
//  Copyright (c) 2014 anime_pictures_net. All rights reserved.
//

#import "SearchFiltersCell.h"
#import "UserInterfaceConstants.h"
@implementation SearchFiltersCell
@synthesize searchView = _searchView;

-(instancetype)init{
    self = [super init];
    [self addSubview:self.searchView];
    [self addConstraintsForSearchView];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    return self;
}

-(SearchFiltersCell*)initWithColorView:(UIView*)colorView{
    self = [self init];
    self.colorView = colorView;
    [self addSubview:self.colorView];
    [self addConstraintsForColorView];
    return self;
}

-(BTSSearchView*)searchView{
    if (!_searchView){
        _searchView = [[BTSSearchView alloc]init];
        _searchView.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _searchView;
}


-(void)addConstraintsForSearchView{
    NSLayoutConstraint *width = [NSLayoutConstraint constraintWithItem:self.searchView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0.0];
    NSLayoutConstraint *height = [NSLayoutConstraint constraintWithItem:self.searchView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:250.0];
    NSLayoutConstraint *centerX = [NSLayoutConstraint constraintWithItem:self.searchView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0];
    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:self.searchView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0];
    [self addConstraints:@[width, height, centerX, top]];
}

-(void)addConstraintsForColorView{
    NSLayoutConstraint *width = [NSLayoutConstraint constraintWithItem:self.colorView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeWidth multiplier:1.0 constant:-HORIZONTAL_PADDING];
    NSLayoutConstraint *centerX = [NSLayoutConstraint constraintWithItem:self.colorView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0];
    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:self.colorView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.searchView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:VERTICAL_PADDING];
    NSLayoutConstraint *bottom = [NSLayoutConstraint constraintWithItem:self.colorView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-VERTICAL_PADDING];
    [self addConstraints:@[top, centerX, bottom, width]];
}

@end
